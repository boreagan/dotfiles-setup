#! /bin/bash

mkdir -p ~/.dotfiles
cd ~/.dotfiles
git init
git config core.worktree .. 
git remote add origin git@gitlab.com:boreagan/dotfiles .
git fetch
git checkout -b linux
git branch --set-upstream-to:origin/macos macos
git pull
