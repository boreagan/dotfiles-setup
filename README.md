# dotfiles-setup

1. Create Directory 

`mkdir -p ~/.dotfiles-setup`

2. If Windows, Hide Folder 

`Get-Item $env:USERPROFILE\.dotfiles-setup | %{ $_.Attributes = $_.Attributes -bor "Hidden" }`

3. Clone Repo

`git clone git@gitlab.com:boreagan/dotfiles-setup.git ./.dotfiles-setup/`

4. Execute Setup Script
